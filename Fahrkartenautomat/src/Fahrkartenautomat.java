﻿import java.util.Locale;
import java.util.Scanner;

class Fahrkartenautomat {

	private static double Fahrscheinmenue(Scanner tastatur) {


		final double[] fahrkartenPreis = { 2.9, 3.3, 3.6, 1.9, 8.60, 9.0, 9.6, 23.50, 24.30, 24.90 };
		final String[] fahrkartenNamen = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
				"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC" };

		System.out.println("Wählen Sie Ihre gewünschte Fahrkarte aus:");
		System.out.println("Bezahlen (0)");
		
		for (int i = 0; i < fahrkartenPreis.length; i++) {
			System.out.printf("%s [%.2f€] (%d)\n", fahrkartenNamen[i], fahrkartenPreis[i], (i + 1));
		}
		
		int nutzerWahl = tastatur.nextInt();
		
		if (nutzerWahl == 0) {
			return -1;
		} 
		else {
			int index = nutzerWahl - 1;
			
			if (index >= fahrkartenPreis.length || index < 0) {
				return 0;
			}
			
			return fahrkartenPreis[index];
		
		}

	}

	private static int ticketsGrenze(Scanner tastatur) {
		int ticketAnzahl = 1;
		boolean isValid = false;

		while (!isValid) {

			System.out.println("Anzahl der Tickets (min: 1, max: 10): ");

			ticketAnzahl = tastatur.nextInt();

			if (ticketAnzahl <= 10 && ticketAnzahl > 0) {
				isValid = true;
			} else {
				System.out.println("Ungültiger Wert !");
			}

		}

		return ticketAnzahl;
	}

	private static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);
		double zuZahlenderBetrag = 0;
		double[] zuZahlendeBetraege = new double[1000];
		int[] ticketsAnzahl = new int[zuZahlendeBetraege.length];
		boolean keineFahrkarteAusgewaelt = true;
		int index = 0;
		
		while(true) {
			double betrag = Fahrscheinmenue(tastatur);
			if(betrag == -1) {
				if(keineFahrkarteAusgewaelt) {
					System.out.println("Sie haben keine Fahrkarten ausgewählt !");
					warte(1000);
					continue;
				}
				else {
					break;
				}
			}
			
			else if (betrag == 0){
				System.out.println("Sie haben eine fasche nummer eingegeben !");
				warte(1000);
				continue;
			}
			else {
				zuZahlendeBetraege[index] = betrag;
				
				int ticketAnzahl = ticketsGrenze(tastatur);
				ticketsAnzahl[index] = ticketAnzahl;
				
				if(keineFahrkarteAusgewaelt) keineFahrkarteAusgewaelt = false;
				
				index++;
			}
		}

		for (int i = 0; i < zuZahlendeBetraege.length; i++) {
			if(zuZahlendeBetraege[i] == 0) break;
			
			zuZahlenderBetrag += (zuZahlendeBetraege[i]*ticketsAnzahl[i]);
		}

		return zuZahlenderBetrag;
	}

	private static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.println(
					String.format(Locale.ROOT, "Noch zu zahlen: %.2f Euro", (zuZahlen - eingezahlterGesamtbetrag)));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");

			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlen;
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	private static void rueckgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				rückgabebetrag -= muenzeAusgeben(2, "EURO");

			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				rückgabebetrag -= muenzeAusgeben(1, "EURO");

			}
			while (rückgabebetrag >= 0.499) // 50 CENT-Münzen
			{
				rückgabebetrag -= muenzeAusgeben(0.5, "CENT");

			}
			while (rückgabebetrag >= 0.199) // 20 CENT-Münzen
			{

				rückgabebetrag -= muenzeAusgeben(0.2, "CENT");

			}
			while (rückgabebetrag >= 0.099) // 10 CENT-Münzen
			{
				rückgabebetrag -= muenzeAusgeben(0.1, "CENT");

			}
			while (rückgabebetrag >= 0.049)// 5 CENT-Münzen
			{

				rückgabebetrag -= muenzeAusgeben(0.05, "CENT");

			}
		}
	}

	private static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static double muenzeAusgeben(double betrag, String einheit) {

		double münze = (betrag < 1.0 ? (betrag * 100) : betrag);

		System.out.printf("%.0f %s%n", münze, einheit);

		return betrag;
	}

	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rückgabebetrag;
		while (true) {

			zuZahlenderBetrag = fahrkartenbestellungErfassen();

			rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();

			rueckgeldAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.");

			System.out.println();
			System.out.println();
			System.out.println("Bitte warten Sie 5 Sekunden, um erneut zu bestellen");
			warte(5000);

		}

	}
}