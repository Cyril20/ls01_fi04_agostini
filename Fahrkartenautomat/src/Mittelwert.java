import java.util.Iterator;
import java.util.Scanner;



public class Mittelwert {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		// (E) "Eingabe"
		// Werte für x und y festlegen:
		// ===========================
		System.out.println("Wie viele Werte wollen Sie eingeben ?");
		int anzahlWerte = tastatur.nextInt();
		double summe = 0;
		double[] zahlen = new double[anzahlWerte];
		
		for (int i = 0; i < anzahlWerte; i++) {
			zahlen[i] = liesDoubleWertEin("Bitte geben Sie einen Werte ein:");
			summe += zahlen[i];
		}
		
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double m = summe / anzahlWerte;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("Der Mittelwert ist %.2f\n", m);
		for (double d : zahlen) {
			System.out.println(d);
		}
	
	}

	private static double liesDoubleWertEin(String frage) {
		Scanner scan = new Scanner(System.in);
		System.out.println(frage);
		double zahl = scan.nextDouble();
		return zahl;

	}


}
