package ABSchleifen;

import java.util.Scanner;

public class Aufgabe8 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben sie bitte eine Ganzzahl ein:");
		
		int anzahl = tastatur.nextInt();
		
		if(anzahl < 2) anzahl = 2;
		
		for (int i = 0; i < anzahl; i++) {
			String text = "";
			
			for (int j = 0; j < anzahl; j++) {
				if(i == 0 || i == anzahl-1) {
					text += "* ";
				}
				else {
					if(j == 0 || j == anzahl-1) {
						text += "* ";	
					}
					else {
						text += "  ";
					}
				}
			}
			text = text.substring(0, text.length()-1);
			System.out.println(text);
		}

	}

}
