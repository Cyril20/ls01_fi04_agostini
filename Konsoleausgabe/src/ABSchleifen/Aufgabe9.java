package ABSchleifen;

import java.util.Scanner;

public class Aufgabe9 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben sie bitte eine Breite (int) ein:");
		int breite = tastatur.nextInt();
		if(breite < 2) breite = 2;
		
		System.out.println("Geben sie bitte eine H�he (int) ein:");
		int hoehe = tastatur.nextInt();
		if(hoehe < 3) hoehe = 3;
		
		for (int i = 1; i < hoehe; i++) {
			String text = "";
			for (int j = 0; j < i; j++) {
				for (int y = 0; y < breite; y++) {
					text += "*";
				}
			}
			int spaces = breite*(hoehe-1);
			System.out.printf("%"+spaces+"s\n", text);
		}

	}

}
