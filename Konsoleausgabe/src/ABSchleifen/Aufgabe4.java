package ABSchleifen;

public class Aufgabe4 {

	public static void main(String[] args) {
		
		System.out.println("a)");
		for (int i = 99; i >= 9; i -= 3) {
			System.out.println(i);
		}
		
		
		System.out.println("\nb)");
		for (int i = 0; i <= 20; i++) {
			if(i <= 1) {
				System.out.println((i*i)+1);
			}
			else {
				System.out.println(i*i);
			}
			
		}
		
		System.out.println("\nc)");
		
		for (int i = 2; i <= 102; i += 4) {
			System.out.println(i);
		}
		
		System.out.println("\nd) ?");
		
		System.out.println("\ne)");
		
		for (int i = 2; i <= 32768; i *= 2) {
			System.out.println(i);
		}

	}

}
