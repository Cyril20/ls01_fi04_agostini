package ABSchleifen;

import java.util.Scanner;

public class Aufgabe6 {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Geben sie bitte eine Ganzzahl ein:");
		int sterne = tastatur.nextInt();
		
		for (int i = 1; i <= sterne; i++) {
			String text = "";
			for (int j = 0; j < i; j++) {
				text += "*";
			}
			System.out.println(text);
		}

	}

}
