package ABSchleifen;

public class Aufgabe7 {

	public static void main(String[] args) {
		
		System.out.println(2);
		
		for (int i = 3; i < 100; i += 2) {
			int geteiltAnzahl = 0;
			
			for (int j = 1; j <= i; j++) {
				if(i % j == 0) geteiltAnzahl++;
				if(geteiltAnzahl > 2) break;
			}
			if(geteiltAnzahl == 2) {
				System.out.println(i);
			}
		}

	}

}
