
public class Aufgabe3 {

	public static void main(String[] args) {

		final int[] fahrenheit = { -20, -10, 0, 20, 30 };
		final double[] celcius = { -28.8889, -23.3333, -17.7778, -6.6667, -1.1111 };

		String spalte1 = "Fahrenheit";
		String spalte2 = "Celcius";
		String linie = "------------------------\n";

		
		System.out.printf("%12s|%10s\n%s", spalte1,spalte2,linie);
		
		for (int i = 0; i < 5; i++) {

			
			System.out.printf("%+-12d|%+10.2f\n", fahrenheit[i], celcius[i]);

		}

	}

}
