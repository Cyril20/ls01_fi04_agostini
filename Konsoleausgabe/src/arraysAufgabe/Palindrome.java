package arraysAufgabe;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		char[] zeichen = new char[5];
		for (int i = 0; i < 5; i++) {
			System.out.println("Geben sie bitte ein zeichen ein (" + (i + 1) + "/5):");
			
			zeichen[i] = tastatur.next().toString().charAt(0);
		}

		for (int i = zeichen.length - 1; i >= 0; i--) {
			System.out.print(zeichen[i] + ", ");
		}

	}

}
