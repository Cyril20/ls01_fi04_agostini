package arraysAufgabe;

public class UngeradeZahlen {

	public static void main(String[] args) {
		int[] zahlen = new int[10];

		int count = 0;
		for (int i = 1; i < 20; i+=2) {
			zahlen[count] = i;
			count++;
		}

		for (int z : zahlen) {
			System.out.print(z + ", ");
		}

	}

}
