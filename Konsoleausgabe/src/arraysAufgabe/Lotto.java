package arraysAufgabe;

public class Lotto {

	public static void main(String[] args) {

		final int[] ergebnis = { 3, 7, 12, 18, 37, 42 };
		
		checkNumber(ergebnis, 12);
		checkNumber(ergebnis, 13);
	}
	
	private static void checkNumber(int[] ergebnis, int number) {
		for (int i = 0; i < ergebnis.length; i++) {
			
			if(ergebnis[i] == number) {
				System.out.printf("Die Zahl %d ist in der Ziehung enthalten.\n", number);
				break;
			}
			if(i == ergebnis.length-1) {
				System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.\n", number);
			}
		}

	
	}

}
